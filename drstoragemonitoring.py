import serial
import json
import argparse
import helpers

parser = argparse.ArgumentParser(
        description = "Read temperature and relative humidity from Dr Storage X2B-400.")

parser.add_argument('-c',
                    dest    = 'configfile',
                    type    = str,
                    default = '',
                    help    = 'Path to configuration json'
                    )

parser.add_argument('-o',
                    dest    = 'logfile',
                    type    = str,
                    default = '',
                    help    = 'Path to output log file'
                    )

args = parser.parse_args()

# path to configuration file
configfile = args.configfile

# path to log file
logfile = args.logfile

# load json from configuration file
try:
    conf = json.load(open(configfile))
except:
    print("Cannot load configfile, using default values")
    conf = dict()

# initiate values from config file or set to default (from X2B-400)
# m -> measured, s -> set, t -> temperature, h -> humidity, idxs -> indices
m_h_idxs = conf.get('Data_Indices', dict()).get('measured_humidity', [2,3])
m_t_idxs = conf.get('Data_Indices', dict()).get('measured_temperature', [5,6])
checksum_idx = conf.get('Data_Indices', dict()).get('checksum', 28)

# Not sure what this quantity is.
unknown_quantity_idxs = conf.get('Data_Indices', dict()).get('unknown_quantity', [8,9])

# these values for temperature and humidity are what is set on the cabinet.
# Not sure why changing these has no effect on the actual temp/humidity.
#s_h_idxs = conf.get('Data_Indicies', dict()).get('set_humidity', [16, 17])
#s_t_idxs = conf.get('Data_Indicies', dict()).get('set_temperature', [19, 20])

# Grafana
grafana_active = conf.get('Grafana', dict()).get('active', False)
grafana_server = conf.get('Grafana', dict()).get('server', "206.12.94.115")
grafana_port   = conf.get('Grafana', dict()).get('port', "8186")
database       = conf.get('Grafana', dict()).get('database', 'telegraf')

# sensor name
sensor = conf.get('DrStorage', dict()).get('model', 'X2B-400')

with serial.Serial("/dev/ttyUSB0") as ser:
   
    # get hex bytes from serial port
    hex_data = ser.read_until(serial.CR + serial.LF)
    # convert hex data into an array of decimal values
    data = [x for x in hex_data]

    # if you didn't receive 31 bytes, try again.
    while (len(data) != 31):
        ser.close()
        ser = serial.Serial("/dev/ttyUSB0")
        hex_data = ser.read_until(serial.CR + serial.LF)
        data = [x for x in hex_data]

    # verify checksum is valid
    checksum_valid = helpers.checksum_valid(data, checksum_idx)
     
    # calculate temperature and humidity from bytes
    humidity    = helpers.calculate_value(data, m_h_idxs)
    temperature = helpers.calculate_value(data, m_t_idxs)
    #set_humidity         = helpers.calculate_value(data, s_h_idxs)
    #set_temperature      = helpers.calculate_value(data, s_t_idxs) 
    
    # still working out what this quantity is, but I think it is the temperature
    # of the desiccator (?)
    unknown_quantity = helpers.calculate_value(data, unknown_quantity_idxs)/10
    

    if (grafana_active and checksum_valid):
        helpers.send2influxdb(grafana_server, grafana_port, sensor, temperature,
                              humidity, database)

    if (grafana_active and checksum_valid):
        helpers.send2influxdb_temp(grafana_server, grafana_port, sensor, unknown_quantity,
                                    database)
    

    byte_4  = data[4]
    byte_7  = data[7]
    byte_18 = data[18]
    byte_21 = data[21]

    if (logfile):
        helpers.logdata(logfile, sensor, temperature, humidity, unknown_quantity, byte_4, byte_7, byte_18, byte_21)
