import os
import time
import requests

def logdata(logfile, sensor, temperature, humidity, unknown_quantity,
            byte_4, byte_7, byte_18, byte_21):
    '''
    logdata( (string) logfile, (string) sensor, (float) temperature, (float) humidity)

    Takes sensor name and data and prints to output file for logging
    '''
    # Append data to existing file, create it if it doesn't exist.
    with open(logfile, 'a+') as f:

        save_string = str(int(time.time())) + ", " + sensor + ", " + str(temperature) +  ", " + str(humidity) + ", " + ", " + str(unknown_quantity) + ", " + str(byte_4) + ", " + str(byte_7) + ", " + str(byte_18) + ", " + str(byte_21)

        f.write(save_string)
        f.write('\n')
        f.close()

def calculate_value(data, idxs):
    '''
    calculate_value( (int array) data, (int array) idxs )
    
    This function returns a 16-bit integer calculated from two 8-bit integers.
    Byte_1 is located in the data array at the first index of the given indices
    and Byte_2 is the second. After converting to a 16-bit number, the value
    must be divided by 10 to get the correct reading.
    '''

    byte_1 = data[idxs[0]]
    byte_2 = data[idxs[1]]

    value = (byte_1*2.0**8 + byte_2)/10.0

    return value

def checksum_valid(data, checksum_index):
    '''
    checksum_valid( (int array) data, (int) checksum_index )

    The function returns a boolean by checking the checksum given in the bytes
    from the Dr Storage, located in the data array at index 28 (at least for
    the X2B-400 model, against the calculated checksum, which is the sum of all
    bytes up to the checksum mod 256.
    '''
    
    checksum_from_data  = data[checksum_index]
    calculated_checksum = sum(data[:checksum_index]) % 256

    return (checksum_from_data == calculated_checksum)

def send2influxdb(server, port, sensor, temperature, humidity, database=""):
    '''
    send2influxdb( (string) server, (string) port, (string) sensor,
                   (float) temperature, (float) humidity, (string) database)
    
    This function takes in the sensor values and sends them to our database. The
    function was taken from Carlos Garcia Argos' script and modifified by Zhengcheng
    Tao, then modified by Cole Helling.
    
    -- server (string)
        IP address to the virtual machine holding the data
    
    -- port (string)
        port number the VM is expecting for data
    
    -- sensor (string)
        name of the sensor
    
    -- temperature (float)
        value extracted from the serial readout of the dry storage
        cabinet
    
    -- humidity (float)
        value extracted from the serial readout of the dry storage
        cabinet
    
    -- database (string)
        name of the database
    '''
    # destination
    argument = "/write?precision=s"

    if (database):
        argument += "&db=" + database

    # via http
    destination = "http://" + server + ":" + port + argument

    # data
    hostname = os.uname()[1]
    timestamp = int(time.time())
    data = "DrStorage,host={},sensor={} temperature={},humidity={} {}".format(hostname,
           sensor, temperature, humidity, timestamp)

    # send data to destination
    requests.post(destination, data=data)

def send2influxdb_temp(server, port, sensor, mystery_value, database=""):
    # destination
    argument = "/write?precision=s"

    if (database):
        argument += "&db=" + database

    # via http
    destination = "http://" + server + ":" + port + argument

    # data
    hostname = os.uname()[1]
    timestamp = int(time.time())
    data = "DrStorage_temp,host={},sensor={} mystery_value={} {}".format(hostname,
           sensor, mystery_value, timestamp)

    # send data to destination
    requests.post(destination, data=data)
