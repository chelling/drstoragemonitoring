# DrStorageMonitoring

This project is designed to read out the temperature and humidity data from the
Dr Storage X2B-400 dry storage cabinet. As of this writing, it is not known if
it will read other models, but if I get some examples of other models, I can
test it out. The project is loosely based on a project by [Giordon
Stark](https://github.com/kratsg/drstorage) which is complemented by a readout
project made by [Matt
Gignac](https://gitlab.cern.ch/scipp/strips/cleanroom-monitoring). I am
thankful to both of them, as well as Zhengcheng Tao, as it made parsing the
byte stream from the cabinet comprehensible.

## Dependencies

The Raspberry Pi communicates with dry storage cabinet via an RS232-to-USB connection.
All packages should already be installed:

  * os
  * time
  * requests
  * serial
  * json
  * argparse

## Configuration

A json is used to store the configuration settings to identify which parts of
the data are temperature and humidity, among a few other things which aren't
actually saved (though perhaps useful for troubleshooting). It also stores the
settings for sending data to the database.

``` json
{
    "Data_Indices": {
        "measured_humidity": [2, 3],
        "measured_temperature": [5, 6],
        "unknown_quantity": [8, 9],
        "set_humidity": [16, 17],
        "set_temperature": [19, 20],
        "checksum": 28
    },
    "Grafana": {
        "active"  : true,
        "server"  : "206.12.94.115",
        "port"    : "8186",
        "database": "telegraf"
    },
    "DrStorage": {
        "model" : "X2B-400"
    }
}
```

The `Data_Indices` field stores the indices where the particular value is
stored in the data array. For the most part, only the `measured_humidity`,
`measured_temperature`, and `checksum` are used. Currently, there is some
unknown quantity which is a mystery to me. This part will be updated once I
figure it out. The indices for the various bytes are discussed in more detail
in the [Data from Dr
Storage](https://gitlab.cern.ch/chelling/drstoragemonitoring#data-from-dr-storage)
section.

We send data to a virtual machine using [InfluxDB](https://www.influxdata.com/)
via the [Telegraf](https://www.influxdata.com/time-series-platform/telegraf/)
server agent. To send data, set the `active` field to `true` and configure the
`server` and `port` fields appropriately.

The model in the `DrStorage` field is just to send that bit of information
along to the database to make it easier to find the values in Grafana.

## Logging Readout to a File

To output the readings to a log file, add `-o [log_output_filename].txt` to the
python command:

```bash
sudo python3 drstoragemonitoring.py -c configuration.json -o output.txt
```

It is currently putting out the time, model, temp, humidity, mystery value, and
the four (two?) indicator bytes (see the [Data from Dr
Storage](https://gitlab.cern.ch/chelling/drstoragemonitoring#data-from-dr-storage)
section for more details on those).

## Running the Program

This program was designed to be called as a cronjob every minute. This is done
by opening up the
[crontab](https://www.adminschoice.com/crontab-quick-reference) (in this case
with vim as the editor) using the command:

``` bash
EDITOR=vim crontab -e
```

The following line will execute the script every minute:

```bash
* * * * * sudo python3 /[path_to_repository]/drstoragemonitoring/drstoragemonitoring.py -c /[path_to_repository]/drstoragemonitoring/configuration.json
```

## Data from Dr Storage

Data can be read from the cabinet by using the serial library, and needs `sudo` to
be able to access the connection:

``` python
import serial
ser = serial.Serial("/dev/ttyUSB0")
hex_data = ser.read_until(serial.CR + serial.LF)
print(hex_data.hex())
```

The `read_until(serial.CR + serial.LF)` command will read the raw data until
the `\r\n` characters. Printing the hex_data will produce:

``` bash
'abab00011000e112092b000000000000000a1000d2100000000000008a0d0a'
```

If you can't access `/dev/ttyUSB0`, enter this into the command line:

```bash
dmesg | grep tty
```

Which should have the following output:

```bash
[    0.000000] Kernel command line: coherent_pool=1M 8250.nr_uarts=0 snd_bcm2835.enable_compat_alsa=0 snd_bcm2835.enable_hdmi=1 bcm2708_fb.fbwidth=720 bcm2708_fb.fbheight=480 bcm2708_fb.fbswap=1 vc_mem.mem_base=0x3ec00000 vc_mem.mem_size=0x40000000  dwc_otg.lpm_enable=0 console=ttyS0,115200 console=tty1 root=PARTUUID=9c9cbb5b-02 rootfstype=ext4 elevator=deadline fsck.repair=yes rootwait quiet splash plymouth.ignore-serial-consoles usbhid.mousepoll=0
[    0.000380] printk: console [tty1] enabled
[    2.968138] 3f201000.serial: ttyAMA0 at MMIO 0x3f201000 (irq = 114, base_baud = 0) is a PL011 rev2
[ 1211.661213] usb 1-1.3: pl2303 converter now attached to ttyUSB0
```

The pl2303 driver is used to convert the serial connection to USB, so whatever
it is said to be attached to (in my case ttyUSB0), use that as the argument in
the `serial.Serial()` call.

If the data is correct, you should receive a total of 31 bytes. From the script
by Giordon mentioned above, there seems to be some commonalities in the data
format between different Dr Storage models, but the X2B-400 seems to differ
enough that I found it easier to write my own version of his program.  While
his is by far more clever, mine works for this model. All bytes recieved from
the various models starts with `abab`, and the data is formatted similarly, but
in the case of the X2B-400, the model number is not nested somewhere in the
datastream. I spent a lot of time staring at the readout and comparing with the
values on the cabinet's display and here is what I've determined about the
data. I'll use the indices to explain which byte is which, starting from byte
0, to byte 30. Since there is no manual that I could find, I'll give each
series of bytes my own naming convention. It's simpler (for me at least), to
talk about the values in decimal rather than hexidecimal. The hex data can be
easily put into an array of byte-sized integers by doing the following to the
`hex_data`:

``` python
data = [x for x in hex_data]
```
which has the follwing output:

``` bash
[171, 171, 0, 1, 16, 0, 225, 18, 9, 43, 0, 0, 0, 0, 0, 0, 0, 10, 16, 0, 210, 16, 0, 0, 0, 0, 0, 0, 138, 13, 10]
```

### Hello Bytes

Bytes 0 and 1 are both the *introductory* bytes, and are always 171.

### Humidity Bytes

Bytes 2 and 3 are the relative humidity bytes. To get the correct value for the
humidity, convert the bytes to a 16-bit number and divide by 10, *i.e.*

``` python
humidity = (byte_2*2**8 + byte_3)/10.0
```

### Humidity Above/Below Setting Byte

Byte 4 is either 16 or 18, *I think* to indicate if the relative humidity is
below or above the humidity set on the cabinet, respectively. 

### Temperature Bytes

Bytes 5 and 6 are the temperature bytes. To get the correct value for the
temperature, convert the bytes to a 16-bit number and divide by 10, *i.e.*

``` python
temperature = (byte_5*2**8 + byte_6)/10.0
``` 

### Temperature Above/Below Setting Byte

Byte 7 is either 16 or 18, *I think* to indicate if the temperature is 
below or above the temperature set on the cabinet, respectively.

### Mystery Bytes

Bytes 8 and 9 are still a mystery to me. It appears to be a temperature reading
(though to two decimal places rather than one). I suspect this is the
temperature of the desiccator, but I'm still working on it. Conversion is
similar to humidity and temperature, but divide by 100 instead of 10, *i.e.*

``` python
temperature = (byte_8*2**8 + byte_9)/100.0
``` 

### Padding Bytes

Bytes 10 through 15 (6 bytes) are zeros. 

### Humidity Setting Bytes

Bytes 16 and 17 are the humidity setting on the storage cabinet. Although,
setting the humidity high doesn't seem to make the humidity in the cabinet
high, so it seems pretty useless so far. This may be an option for some models
and not others. Convert the bytes to a 16-bit number in the same manner as the
humidity reading.

### Spacer/Indicator Byte

Byte 18 is so far always 16. No matter which settings I toggle it doesn't change
to 18. I'm still investigating this.

### Temperature Setting Bytes

Bytes 19 and 20 are the temperature setting on the storage cabinet. Currently,
changing this doesn't affect the temperature of the cabinet. This may be an
option for some models and not others.

### Spacer/Indicator Byte

Byte 21 is so far always 16. No matter which settings I toggle it doesn't
change to 18. I'm still investigating this.

### Padding Bytes

Bytes 22 through 27 (6 bytes) are zeros.

### Checksum Byte

Byte 28 is the checksum. You can verify the checksum by summing all of the data
(including the introductory bytes) up to but not including the checksum value
mod 256, *i.e*

``` python
checksum = sum(data[:28]) % 256
```
### Goodbye bits

Bytes 29 and 30 are 13 and 10, respectively. They signify the end of the data
transmission.

